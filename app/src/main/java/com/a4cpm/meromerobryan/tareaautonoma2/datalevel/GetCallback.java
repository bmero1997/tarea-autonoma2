package com.a4cpm.meromerobryan.tareaautonoma2.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
